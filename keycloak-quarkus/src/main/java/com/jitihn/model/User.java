package com.jitihn.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.jitihn.dto.UserCredentials;
import com.jitihn.dto.UserDTO;

public class User {

    private String username;
    private String email;
    private Boolean enabled;
    private List<String> realmRoles;
    private List<UserCredentials> credentials;

    public User(UserDTO dto) {
        this.username = dto.getUsername();
        this.email = dto.getEmail();
        this.enabled = dto.getEnabled();
        this.realmRoles = dto.getRealmRoles();
        this.credentials = parsePass(dto.getPassword());
    }

    List<UserCredentials> parsePass(String pass) {
        List<UserCredentials> credList = new ArrayList<>();
        UserCredentials cred = new UserCredentials("password", false, pass);
        credList.add(cred);
        return credList;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public List<String> getRealmRoles() {
        return realmRoles;
    }

    public void setRealmRoles(List<String> realmRoles) {
        this.realmRoles = realmRoles;
    }

    public User() {
    }

    public List<UserCredentials> getCredentials() {
        return credentials;
    }

    public void setCredentials(List<UserCredentials> credentials) {
        this.credentials = credentials;
    }
}