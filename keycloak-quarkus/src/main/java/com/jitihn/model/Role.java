package com.jitihn.model;

import com.jitihn.dto.RoleDTO;

public class Role {

    private String roleId;
    private String roleName;
    private String description;
    private String realmId;

    public Role(RoleDTO roleDTO) {
        this.roleId = roleDTO.getRoleName();
        this.description = roleDTO.getDescription();
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRealmId() {
        return realmId;
    }

    public void setRealmId(String realmId) {
        this.realmId = realmId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public Role() {
    }

}