package com.jitihn.service;

import java.util.Set;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.jitihn.model.Realm;
import com.jitihn.model.Role;
import com.jitihn.model.User;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

@Path("/keycloak")
@RegisterRestClient
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface KeycloakService {

    @GET
    @Path("/realm")
    Response getRealms();

    @GET
    @Path("/realm/{realmId}")
    Response getRealm(@PathParam String realmId);

    @POST
    @Path("/realm")
    Response createRealm(Realm realm);

    @DELETE
    @Path("/realm/{realmId}")
    Response deleteRealm(@PathParam String realmId);

    // http://localhost:8080/api/keycloak/keycloak-client/role

    @POST
    @Path("/{realmname}/role")
    Response createRole(@PathParam String realmname, Role role);

    @GET
    @Path("/{realmname}/role/{role_name}")
    Response getRole(@PathParam String realmname, @PathParam String role_name);

    @DELETE
    @Path("/{realmname}/role/{role_name}")
    Response deleteRole(@PathParam String realmname, @PathParam String role_name);

    @GET
    @Path("/{realmname}/role")
    Response getRole(@PathParam String realmname);

    // http://localhost:8080/api/keycloak/keycloak-client/user

    @POST
    @Path("/{realmname}/user")
    Response createUser(@PathParam String realmname, User user);

    @GET
    @Path("/{realmname}/user")
    Response getUser(@PathParam String realmname);

    @GET
    @Path("/{realmname}/user/{username}")
    Response getUser(@PathParam String realmname, @PathParam String username);

    // http://localhost:8080/api/keycloak/MyNewCompany/user/009430cc-08ee-4ba4-83b9-45cbfc7fc67a
    @DELETE
    @Path("/{realmname}/user/{user_id}")
    Response deleteUser(@PathParam("realmname") String company_name, @PathParam("user_id") String user_id);
    // @DELETE
    // @Path("/{realmname}/user/{user_name}")
    // Response deleteAUser(String company_name, String user_name);
}