package com.jitihn.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import com.jitihn.dto.RoleDTO;
import com.jitihn.dto.UserDTO;
import com.jitihn.model.Company;
import com.jitihn.model.Realm;
import com.jitihn.service.KeycloakService;
import com.jitihn.utils.Utils;

import org.eclipse.microprofile.rest.client.inject.RestClient;

@Path("/api/company")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CompanyResource {

    @Inject
    @RestClient
    KeycloakService keycloakService;

    @Inject
    RoleResource roleResource;

    @Inject
    UserResource userResource;

    // @GET
    // public Response get() {
    // return Response.ok("company").build();
    // }

    @GET
    @Path("/apis")
    public Response apis(@Context UriInfo uri) {
        String baseUri = uri.getBaseUri().toString() + "api/company";
        HashMap<String, Object> obj = new HashMap<>();
        List<String> apis = new ArrayList<>();
        apis.add("GET : " + baseUri);
        apis.add("GET : " + baseUri + "/apis");
        apis.add("GET : " + baseUri + "/company_id");
        apis.add("DELETE : " + baseUri + "/company_id");
        apis.add("POST : " + baseUri);
        obj.put("data", apis);
        return Utils.JsonResponse(Status.OK, "api list", true, obj);
    }

    @GET
    public Response list() {
        return keycloakService.getRealms();
    }

    @GET
    @Path("/{company_id}")
    public Response get(@PathParam("company_id") String company_id) {
        return keycloakService.getRealm(company_id);
    }

    @DELETE
    @Path("/{company_id}")
    public Response delete(@PathParam("company_id") String company_id) {
        return keycloakService.deleteRealm(company_id);
    }

    @POST
    public Response create(Company company) {
        Realm realm = new Realm();
        realm.setRealm(company.getName());
        Response res = keycloakService.createRealm(realm);
        if (res.getStatus() == 200) {

            Response roleRes = roleResource.get(company.getName(), "admin");

            if (roleRes.getStatus() != 200) {
                // create admin role
                RoleDTO roleDTO = new RoleDTO();
                roleDTO.setCompanyName(company.getName());
                roleDTO.setRoleName("admin");
                roleResource.create(roleDTO);
            }

            UserDTO userDTO = new UserDTO();
            userDTO.setCompanyName(company.getName());
            userDTO.setUsername(company.getAdmin_name());
            userDTO.setEmail(company.getAdmin_email());
            userDTO.setRealmRoles(Arrays.asList("admin"));
            userDTO.setPassword(company.getAdmin_pass());
            userDTO.setEnabled(true);
            userResource.create(userDTO);

            Company c = new Company().parseRealm(res.readEntity(Realm.class));
            c.setAdmin_name(company.getAdmin_name());
            c.setAdmin_email(company.getAdmin_email());
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", c);
            return Utils.JsonResponse(Status.OK, "company", true, obj);
        }

        return Utils.JsonResponse(Status.BAD_REQUEST, "company name required", false, null);
    }
}