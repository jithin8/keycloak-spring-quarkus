package com.jitihn.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;
import com.jitihn.dto.UserDTO;
import com.jitihn.model.User;
import com.jitihn.service.KeycloakService;
import com.jitihn.utils.Utils;

import org.eclipse.microprofile.rest.client.inject.RestClient;

@Path("/api/user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {

    @Inject
    @RestClient
    KeycloakService keycloakService;

    // @GET
    // public Response get() {
    // return Response.ok("company").build();
    // }

    @GET
    @Path("/apis")
    public Response apis(@Context UriInfo uri) {
        String baseUri = uri.getBaseUri().toString() + "api/company";
        HashMap<String, Object> obj = new HashMap<>();
        List<String> apis = new ArrayList<>();
        apis.add("GET : " + baseUri);

        obj.put("data", apis);
        return Utils.JsonResponse(Status.OK, "api list", true, obj);
    }

    @POST
    public Response create(UserDTO userDTO) {
        // return Response.ok(new User(userDTO)).build();
        Response res = keycloakService.createUser(userDTO.getCompanyName(), new User(userDTO));
        if (res.getStatus() == 200) {
            // create admin
            return Utils.JsonResponse(Status.OK, "user created", true, null);
        }

        return Utils.JsonResponse(Status.BAD_REQUEST, "something wrong", false, null);
    }

    @GET
    @Path("/{company_name}")
    public Response get(@PathParam("company_name") String company_name) {
        return keycloakService.getUser(company_name);
    }

    @GET
    @Path("/{company_name}/{user_name}")
    public Response getUser(@PathParam("company_name") String company_name, @PathParam("user_name") String user_name) {
        return keycloakService.getUser(company_name, user_name);
    }

    @DELETE
    @Path("/{company_name}/{user_id}")
    public Response deleteUser(@PathParam("company_name") String company_name, @PathParam("user_id") String user_id) {
        return keycloakService.deleteUser(company_name, user_id);
        // return Response.ok(user_id).build();
    }

}