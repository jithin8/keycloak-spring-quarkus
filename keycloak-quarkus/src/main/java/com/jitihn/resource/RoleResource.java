package com.jitihn.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import com.jitihn.dto.RoleDTO;
import com.jitihn.model.Company;
import com.jitihn.model.Realm;
import com.jitihn.model.Role;
import com.jitihn.service.KeycloakService;
import com.jitihn.utils.Utils;

import org.eclipse.microprofile.rest.client.inject.RestClient;

@Path("/api/role")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RoleResource {

    @Inject
    @RestClient
    KeycloakService keycloakService;

    // @GET
    // public Response get() {
    // return Response.ok("company").build();
    // }

    @GET
    @Path("/apis")
    public Response apis(@Context UriInfo uri) {
        String baseUri = uri.getBaseUri().toString() + "api/company";
        HashMap<String, Object> obj = new HashMap<>();
        List<String> apis = new ArrayList<>();
        apis.add("GET : " + baseUri);

        obj.put("data", apis);
        return Utils.JsonResponse(Status.OK, "api list", true, obj);
    }

    @GET
    @Path("/{company_name}/{role_name}")
    public Response get(@PathParam("company_name") String company_name, @PathParam("role_name") String role_name) {
        return keycloakService.getRole(company_name, role_name);
    }

    @DELETE
    @Path("/{company_name}/{role_name}")
    public Response delete(@PathParam("company_name") String company_name, @PathParam("role_name") String role_name) {
        return keycloakService.deleteRole(company_name, role_name);
    }

    @GET
    @Path("/{company_name}")
    public Response getRoles(@PathParam("company_name") String company_name) {
        return keycloakService.getRole(company_name);
    }

    @POST
    public Response create(RoleDTO roleDTO) {
        Response res = keycloakService.createRole(roleDTO.getCompanyName(), new Role(roleDTO));
        if (res.getStatus() == 200) {
            // create admin

            Role role = res.readEntity(Role.class);
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", new RoleDTO(role));
            return Utils.JsonResponse(Status.OK, "role created", true, obj);
        }

        return Utils.JsonResponse(Status.BAD_REQUEST, "roleId required", false, null);
    }
}