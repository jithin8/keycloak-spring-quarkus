package com.jitihn.dto;

public class UserCredentials {
    private String type;
    private Boolean temporary;
    private String value;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getTemporary() {
        return temporary;
    }

    public void setTemporary(Boolean temporary) {
        this.temporary = temporary;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public UserCredentials(String type, Boolean temporary, String value) {
        this.type = type;
        this.temporary = temporary;
        this.value = value;
    }

    public UserCredentials() {
    }
}