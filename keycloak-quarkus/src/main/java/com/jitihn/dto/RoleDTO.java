package com.jitihn.dto;

import com.jitihn.model.Role;

public class RoleDTO {

    private String id;
    private String roleName;
    private String companyId;
    private String companyName;
    private String description;

    public RoleDTO(Role role) {
        this.id = role.getRoleId();
        this.roleName = role.getRoleName();
        this.companyId = role.getRealmId();
        this.description = role.getDescription();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public RoleDTO() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

}