package com.jithin.springkeycloak.dtos;

import java.util.List;

import org.keycloak.representations.idm.CredentialRepresentation;

public class UserDTO {

    private String id;
    private String username;
    private String email;
    private List<String> realmRoles;
    private List<CredentialRepresentation> credentials;
    private Boolean enabled;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<CredentialRepresentation> getCredentialRepresentation() {
        return credentials;
    }

    public void setCredentialRepresentation(List<CredentialRepresentation> credentials) {
        this.credentials = credentials;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getRealmRoles() {
        return realmRoles;
    }

    public void setRealmRoles(List<String> realmRoles) {
        this.realmRoles = realmRoles;
    }

    public List<CredentialRepresentation> getCredentials() {
        return credentials;
    }

    public void setCredentials(List<CredentialRepresentation> credentials) {
        this.credentials = credentials;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}