package com.jithin.springkeycloak.controller.keycloak;

import java.util.List;
import com.jithin.springkeycloak.dtos.RealmDTO;
import com.jithin.springkeycloak.exception.ApiRequestException;
import com.jithin.springkeycloak.service.KeycloakService;
import org.keycloak.representations.idm.RealmRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/keycloak")
public class RealmController {

    private KeycloakService keycloakService;

    @Autowired
    public RealmController(KeycloakService keycloakService) {
        this.keycloakService = keycloakService;
    }

    @GetMapping("/realm")
    public List<RealmDTO> getRealm() {
        return keycloakService.getRealms();
    }

    @GetMapping("/realmraw ")
    public List<RealmDTO> getRealmRaw() {
        return keycloakService.getRealms();
    }

    @GetMapping("/realm/{realmId}")
    public RealmDTO getRealmById(@PathVariable String realmId) {
        return keycloakService.getRealmById(realmId);
    }

    @DeleteMapping("/realm/{realmId}")
    public ResponseEntity deleteByRealmId(@PathVariable String realmId) {
        keycloakService.deleteById(realmId);
        return ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @GetMapping("/realmraw")
    public List<RealmRepresentation> getRawRealm() {
        return keycloakService.getRawRealms();
    }

    @PostMapping("/realm")
    @ResponseBody
    RealmDTO createRealm(@RequestBody RealmDTO realmDTO) {
        if (realmDTO == null)
            throw new ApiRequestException("realm name required");
        if (realmDTO.getRealm() == null || realmDTO.getRealm().length() == 0)
            throw new ApiRequestException("realm name required");
        keycloakService.createRealm(realmDTO.getRealm());
        return keycloakService.getRealmById(realmDTO.getRealm());
    }

}