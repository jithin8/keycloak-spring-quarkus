package com.jithin.springkeycloak.controller.keycloak;

import java.util.List;

import com.jithin.springkeycloak.dtos.UserDTO;
import com.jithin.springkeycloak.service.KeycloakService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/keycloak/")
public class UserController {
    private KeycloakService keycloakService;

    @Autowired
    public UserController(KeycloakService keycloakService) {
        this.keycloakService = keycloakService;
    }

    @GetMapping("{realmId}/user")
    public List<UserDTO> getRole(@PathVariable String realmId) {
        return keycloakService.getUsers(realmId);
    }

    @PostMapping("{realmId}/user")
    @ResponseBody
    Boolean createUser(@PathVariable String realmId, @RequestBody UserDTO userDTO) {
        boolean status = keycloakService.createUser(realmId, userDTO);
        return status;
    }

    @GetMapping("{realmId}/user/{userId}")
    @ResponseBody
    UserDTO getUser(@PathVariable String realmId, @PathVariable String userId) {
        return keycloakService.getUser(realmId, userId);
    }

    @DeleteMapping("{realmId}/user/{userId}")
    public ResponseEntity deleteByRoleId(@PathVariable String realmId, @PathVariable String userId) {
        keycloakService.deleteUserById(realmId, userId);
        return ResponseEntity.status(HttpStatus.OK).body(true);
    }
}